﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        private Form1 m_parent = null;
        //public Form2()
        public Form2(Form1 f)
        {
            InitializeComponent();
            m_parent = f;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
